from app import app
from app.controller import ReportController

@app.route("/api/report/<from_date>")
def get_report(from_date):
    return ReportController.get(from_date)
