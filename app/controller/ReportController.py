import datetime
import requests
import os
import json
import urllib

def get(from_date):
    appsflyer_api = "https://hq.appsflyer.com/export/"
    api_token = "3dbe32f2-3756-49f0-a6c0-3e272303f7c5"
    app_id = "com.bitsmedia.android.muslimpro"
    report_name = "partners_by_date_report"

    from_dt = datetime.datetime.strptime(from_date, '%Y-%m-%d').date()
    to_dt = datetime.date.today()

    query_params = {
        "api_token": api_token,
        "from": str(from_dt),
        "to": str(to_dt)
    }

    query_string = urllib.parse.urlencode(query_params)
    request_url = appsflyer_api + app_id + "/" + report_name + "/v5?" + query_string

    print(request_url)

    try:
        resp = urllib.request.urlopen(request_url)


        with open("appsflyer_installs_data.csv", "wb") as fl:
            fl.write(resp.read())
        return "Report downloaded"
    except:
        print("Cannot open url")


